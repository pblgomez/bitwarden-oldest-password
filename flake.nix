{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/";
  };
  outputs = {nixpkgs, ...}: let
    systems = ["x86_64-linux" "aarch64-linux" "aarch64-darwin"];
    forEachSystem = f:
      nixpkgs.lib.genAttrs systems (system:
        f (import nixpkgs {
          inherit system;
          allowUnfree = true;
        }));
    commonDeps = pythonPackages: with pythonPackages; [boto3];
  in {
    devShells = forEachSystem (pkgs: {
      default = pkgs.mkShell {
        buildInputs = [
          (commonDeps pkgs.python311Packages)
          pkgs.go-task
          pkgs.bitwarden-cli
        ];
        shellHook = ''
          nix run nixpkgs#glow README.md
        '';
      };
    });
  };
}
