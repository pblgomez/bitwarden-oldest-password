#!/usr/bin/env python3

import json
from datetime import datetime
from os import remove


def main():
    with open("data.json", "r") as file:
        data = json.load(file)

    def string_to_datetime(date_string):
        return datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%fZ")

    # Convert all revision date strings to datetime objects and create a dictionary
    date_dict = {string_to_datetime(item["revisionDate"]): item for item in data}

    # Find the oldest date
    oldest_date = min(date_dict)

    # Get the item with the oldest date
    oldest_item = date_dict[oldest_date]

    # Convert the oldest date back to a string
    oldest_date_string = oldest_date.strftime("%Y-%m-%dT%H:%M:%S.%fZ")

    # Print the title and the date of the item with the oldest date
    print(f"Name of the item with the oldest date: {oldest_item['name']}\nOldest date: {oldest_date_string}")

    remove("data.json")


if __name__ == "__main__":
    main()
